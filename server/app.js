/*
 * @Description:
 * @Author: mingxiangxiong
 * @Date: 2021-02-10 23:26:17
 * @LastEditors: mingxiangxiong
 * @LastEditTime: 2021-04-01 09:46:35
 */
const express = require('express')
const app = express()

app.all('*', function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
  res.header("X-Powered-By", ' 3.2.1')
  res.header("Content-Type", "application/json;charset=utf-8");
  next();
});

app.listen(3000, () => {
  console.log('3000端口监听开始啦');
})

app.get('/xichengqu', (req, res) => {
  res.json(require('./xicheng.json'))
})

app.get('/japan', (req, res) => {
  res.json(require('./japan.json'))
})

app.get('/england', (req, res) => {
  res.json(require('./england.json'))
})

app.get('/jjhu', (req, res) => {
  res.json(require('./jjLake.json'))
})
